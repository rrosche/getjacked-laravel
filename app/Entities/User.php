<?php

namespace App\Entities;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable {
    use Notifiable;

    protected $primaryKey = "uid";
    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'uid','username', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    /**
     * returns workouts in the user's routine
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function workouts(){
        return $this->belongsToMany(UserWorkout::class, 'user_workouts','uid','workoutId');
    }

    /**
     * returns programs in the user's routine
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function programs(){
        return $this->belongsToMany(Program::class, 'user_programs','uid','programId');
    }
}
