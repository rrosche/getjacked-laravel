<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class MuscleGroup extends Model
{
    public $timestamps = false;
}
