<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Workout extends Model {

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function type(){
        return $this->hasOne(WorkoutType::class, 'id');
    }

    /**
     * returns the exercises in the workout
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function exercises(){
        return $this->belongsToMany(Exercise::class,'workout_contents','workoutId','exerciseId')->withTimestamps();
    }

    /**
     * returns program that contain this workout
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function getContainingPrograms(){
        return $this->belongsToMany(Program::class, 'program_contents','workoutId','programId')->withTimestamps();
}
}
