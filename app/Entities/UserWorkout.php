<?php

namespace App\Entities;

class UserWorkout extends Workout {


    protected $table = "user_workouts";

    /**
     * Gets parent workout
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function details(){
        return $this->belongsTo(Workout::class,'workoutId');
    }

    /**
     * returns the exercises in the workout
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function exercises(){
        return $this->belongsToMany(Exercise::class,'user_workout_contents','workoutId','exerciseId');
    }

    /**
     * returns program that contain this workout
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function getContainingPrograms(){
        return $this->belongsToMany(Program::class, 'user_programs','workoutId','programId');
}
}
