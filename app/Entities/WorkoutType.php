<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class WorkoutType extends Model {

    public $timestamps = false;

}
