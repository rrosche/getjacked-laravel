<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class ProgramType extends Model {
    public $timestamps = false;
}
