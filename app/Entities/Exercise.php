<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Exercise extends Model
{

    /**
     * Returns type of exercise
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function type(){
        return $this->hasOne(ExerciseType::class,'id');
    }

    /**
     * returns muscle groups worked by this exercise
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function muscleGroup(){
        return $this->hasOne(MuscleGroup::class,'id');
    }

    /**
     * returns workouts that contain this exercise
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function getContainingWorkouts(){
        return $this->belongsToMany(Workout::class,'workout_contents','exerciseId','workoutId')->withTimestamps();
    }
}
