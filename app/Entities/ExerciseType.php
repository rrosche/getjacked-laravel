<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class ExerciseType extends Model {
    public $timestamps = false;
}
