<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class ExerciseStat extends Model
{
    public function sets(){
        return $this->hasMany(ExerciseSet::class,'exerciseStatId');
    }
}
