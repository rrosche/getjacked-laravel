<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Program extends Model {


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function type(){
        return $this->hasOne(ProgramType::class,'id');
    }


    /**
     * returns workouts for this program
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function workouts(){
        return $this->belongsToMany(Workout::class, 'program_contents', 'programId', 'workoutId')->withTimestamps();
    }
}
