<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class ExerciseSet extends Model {

    public function stat(){
        $this->belongsTo(ExerciseStat::class,'exerciseStatId');
    }
}
