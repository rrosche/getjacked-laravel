<?php

namespace Tests\Unit;

use App\Entities\ExerciseType;
use App\Entities\MuscleGroup;
use App\Entities\ProgramType;
use App\Entities\WorkoutType;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ModelTypeTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExerciseType(){
        $exerciseType = factory(ExerciseType::class)->create();
        $this->assertDatabaseHas('exercise_types',[
            'id'=>$exerciseType->id,
            'name' => $exerciseType->name
        ]);
    }

    /**
     * Test muscle group model creation
     */
    public function testMuscleGroup(){
        $muscleGroup = factory(MuscleGroup::class)->create();
        $this->assertDatabaseHas('muscle_groups',[
            'id' => $muscleGroup->id,
            'name' => $muscleGroup->name
        ]);
    }

    public function testWorkoutType(){
        $workoutType = factory(WorkoutType::class)->create();
        $this->assertDatabaseHas('workout_types',[
            'id'=>$workoutType->id,
            'name' => $workoutType->name,
        ]);
    }

    public function testProgramType(){
        $programType = factory(ProgramType::class)->create();
        $this->assertDatabaseHas('program_types',[
            'id'=>$programType->id,
            'name' => $programType->name
        ]);
    }

}
