<?php

namespace Tests\Unit;

use App\Entities\Exercise;
use App\Entities\Workout;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ExerciseTest extends TestCase {
    use DatabaseMigrations;

    /**
     * Test exercise Creation
     */
    public function testExcerciseCreation(){
        $exercise = factory(Exercise::class)->create();
        $this->assertDatabaseHas('exercises', [
            'id'=>$exercise->id,
            'name' => $exercise->name,
            'description' => $exercise->description
        ]);

        $this->assertDatabaseHas('exercise_types', ['id'=>$exercise->type]);
        $this->assertDatabaseHas('muscle_groups',['id'=>$exercise->muscleGroup]);
        $this->assertDatabaseHas('users', ['uid'=>$exercise->creatorId]);
    }

    /**
     * Test exercise type relation
     */
    public function testExcerciseType(){
        $exercise = factory(Exercise::class)->create();

        $exerciseDb = Exercise::find($exercise->id);
        $this->assertNotNull($exerciseDb);
        $exerciseType = $exerciseDb->type()->first();
        $this->assertNotEmpty($exerciseType->name);
        $this->assertEquals($exercise->type, $exerciseType->id);
    }

    /**
     * Test exercise muscle group relation
     */
    public function testMuscleGroup(){
        $exercise = factory(Exercise::class)->create();

        $exerciseDb = Exercise::find($exercise->id);
        $this->assertNotNull($exerciseDb);
        $exerciseMuscleGroup = $exerciseDb->muscleGroup()->first();
        $this->assertNotEmpty($exerciseMuscleGroup->name);
        $this->assertEquals($exerciseMuscleGroup->id, $exercise->muscleGroup);
    }

    /**
     * Test inverse relation of exercise to workout
     */
    public function testContainingWorkouts(){
        $exercise = factory(Exercise::class)->create();
        $workout = factory(Workout::class)->create();
        $workout->exercises()->sync([$exercise->id], false);

        $exerciseDb = Exercise::find($exercise->id);
        $workouts = $exerciseDb->getContainingWorkouts();

        $this->assertEquals(1,$workouts->count());

        $this->assertEquals($workout->name, $workouts->first()->name);

        $workoutTwo = factory(Workout::class)->create();
        $workoutTwo->exercises()->sync([$exercise->id],false);

        $this->assertEquals(2, $exerciseDb->getContainingWorkouts()->count());
    }
}
