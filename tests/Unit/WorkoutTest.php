<?php

namespace Tests\Unit;

use App\Entities\Exercise;
use App\Entities\Program;
use App\Entities\Workout;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class WorkoutTest extends TestCase {

    use DatabaseMigrations;

    /**
     * Test workout creation
     */
    public function testWorkoutCreation(){
        $workout = factory(Workout::class)->create();
        $this->assertDatabaseHas('workouts',[
            'id'=>$workout->id,
            'name' => $workout->name,
            'description' => $workout->description
        ]);

        $this->assertDatabaseHas('workout_types', ['id'=>$workout->type]);
        $this->assertDatabaseHas('users',['uid'=>$workout->creatorId]);
    }

    /**
     * Test workout type relation
     */
    public function testWorkoutType(){
        $workout = factory(Workout::class)->create();

        $workoutDb = Workout::find($workout->id);
        $this->assertNotNull($workoutDb);
        $workoutType = $workoutDb->type()->first();
        $this->assertNotEmpty($workoutType->name);
        $this->assertEquals($workout->type, $workoutType->id);
    }

    /**
     * Test workout to exercise relation creation
     */
    public function testWorkoutExercises(){
        $exercise = factory(Exercise::class)->make();
        $workout = factory(Workout::class)->create();
        $workout->exercises()->save($exercise);

        $this->assertDatabaseHas('exercises',['id'=>$exercise->id]);


        $workoutDb = Workout::find($workout->id);

        $exercisesDb = $workoutDb->exercises();

        $this->assertEquals($exercisesDb->count(), 1);
        $this->assertEquals($exercisesDb->first()->id, $exercise->id);
    }

    /**
     * Test inverse relation of exercise to workout
     */
    public function testContainingPrograms(){
        $workout = factory(Workout::class)->create();
        $program = factory(Program::class)->create();
        $program->workouts()->sync([$workout->id], false);

        $workoutDb = Workout::find($workout->id);
        $programs = $workoutDb->getContainingPrograms();

        $this->assertEquals(1,$programs->count());

        $this->assertEquals($program->name, $programs->first()->name);

        $programTwo = factory(Program::class)->create();
        $programTwo->workouts()->sync([$workout->id],false);

        $this->assertEquals(2, $workoutDb->getContainingPrograms()->count());
    }

}
