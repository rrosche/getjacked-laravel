<?php

namespace Tests\Unit;


use App\Entities\Program;
use App\Entities\Workout;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;


class ProgramTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * Test Program creation
     */
    public function testProgramCreation(){
        $program = factory(Program::class)->create();
        $this->assertDatabaseHas('programs',[
            'id'=>$program->id,
            'name' => $program->name,
            'description' => $program->description
        ]);

        $this->assertDatabaseHas('program_types', ['id'=>$program->type]);
        $this->assertDatabaseHas('users',['uid'=>$program->creatorId]);
    }

    /**
     * Test Program type relation
     */
    public function testProgramType(){
        $program = factory(Program::class)->create();

        $programDb = Program::find($program->id);
        $this->assertNotNull($programDb);
        $programType = $programDb->type()->first();
        $this->assertNotEmpty($programType->name);
        $this->assertEquals($program->type, $programType->id);
    }

    /**
     * Test Program to exercise relation creation
     */
    public function testProgramExercises(){
        $workout = factory(Workout::class)->make();
        $program = factory(Program::class)->create();
        $program->workouts()->save($workout);

        $this->assertDatabaseHas('workouts',['id'=>$workout->id]);


        $programDb = Program::find($program->id);

        $workoutsDb = $programDb->workouts();

        $this->assertEquals($workoutsDb->count(), 1);
        $this->assertEquals($workoutsDb->first()->id, $workout->id);
    }
}
