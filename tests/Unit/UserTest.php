<?php

namespace Tests\Unit;

use App\Entities\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class UserTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * Test create user.
     *
     * @return void
     */
    public function testCreate()
    {
        $user = User::create([
            'uid' => uniqid('',true),
            'username' => 'testUser',
            'password' => bcrypt('somepass'),
            'email' => 'tester@testerpass.com',
        ]);

        $this->assertDatabaseHas('users',['uid'=>$user->uid]);
    }
}
