<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class InitAppTest extends TestCase
{
    
    use DatabaseMigrations;
    
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        $this->assertTrue(true);
    }
    
    /**
     * A basic database test example.
     *
     * @return void
     */
    public function testDatabaseTest()
    {
        $user = factory(\App\Entities\User::class)->create();
        $this->assertDatabaseHas('users', [
            'uid' => $user->uid
        ]);
    }
}
