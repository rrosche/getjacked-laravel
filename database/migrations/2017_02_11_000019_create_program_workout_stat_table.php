<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProgramWorkoutStatTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('program_workout_stats', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('programStatId')->unsigned()->index();
			$table->integer('workoutStatId')->unsigned()->index();
			$table->timestamps();

            $table->foreign('programStatId')->references('id')->on('program_stats')->onDelete('cascade');
            $table->foreign('workoutStatId')->references('id')->on('workout_stats')->onDelete('cascade');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('program_workout_stats');
	}

}
