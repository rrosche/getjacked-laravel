<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExercisesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exercises', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 45);
            $table->integer('type')->unsigned()->index();
            $table->integer('muscleGroup')->unsigned()->index();
            $table->text('description', 65535)->nullable();
            $table->timestamp('created_TS')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->string('creatorId', 46)->index();
            $table->timestamps();

            $table->foreign('creatorId')->references('uid')->on('users');
            $table->foreign('type')->references('id')->on('exercise_types');
            $table->foreign('muscleGroup')->references('id')->on('muscle_groups');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exercises');
    }
}
