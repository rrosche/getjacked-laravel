<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserWorkoutContentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_workout_contents', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('workoutId')->unsigned()->index();
			$table->integer('exerciseId')->unsigned()->index();

            $table->foreign('workoutId')->references('id')->on('user_workouts')->onDelete('cascade');
            $table->foreign('exerciseId')->references('id')->on('exercises')->onDelete('cascade');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_workout_contents');
	}

}
