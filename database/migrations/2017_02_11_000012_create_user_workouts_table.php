<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserWorkoutsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_workouts', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('uid',46)->index();
			$table->integer('workoutId')->unsigned()->index();
			$table->string('Schedule', 45);
            $table->timestamps();

            $table->foreign('uid')->references('uid')->on('users')->onDelete('cascade');
            $table->foreign('workoutId')->references('id')->on('workouts')->onDelete('cascade');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_workouts');
	}

}
