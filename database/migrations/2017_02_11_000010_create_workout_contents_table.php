<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateWorkoutContentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('workout_contents', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('workoutId')->unsigned()->index();
			$table->integer('exerciseId')->unsigned()->index();
            $table->timestamps();

            $table->foreign('workoutId')->references('id')->on('workouts');
            $table->foreign('exerciseId')->references('id')->on('exercises');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('workout_contents');
	}

}
