<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProgramContentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('program_contents', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('workoutId')->unsigned()->index();
			$table->integer('programId')->unsigned()->index();
            $table->timestamps();

            $table->foreign('workoutId')->references('id')->on('workouts');
            $table->foreign('programId')->references('id')->on('programs');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('program_contents');
	}

}
