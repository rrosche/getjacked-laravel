<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserProgramsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_programs', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('uid',45)->index();
			$table->integer('programId')->unsigned()->index();

            $table->foreign('uid')->references('uid')->on('users')->onDelete('cascade');
            $table->foreign('programId')->references('id')->on('programs')->onDelete('cascade');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_programs');
	}

}
