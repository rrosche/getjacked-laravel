<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProgramsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        Schema::create('programs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 45);
            $table->integer('type')->unsigned()->index();
            $table->text('description', 65535);
            $table->integer('length')->comment('Days per week');
            $table->string('schedule', 45);
            $table->timestamp('created_TS')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->string('creatorId', 46)->index();
            $table->timestamps();

            $table->foreign('creatorId')->references('uid')->on('users');
            $table->foreign('type')->references('id')->on('program_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        Schema::dropIfExists('programs');
    }
}
