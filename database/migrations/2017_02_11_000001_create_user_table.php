<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->string('uid', 46)->unique();
			$table->string('password');
			$table->string('username', 30)->unique();
			$table->string('firstName', 20)->nullable();
			$table->string('lastName', 20)->nullable();
			$table->date('dateOfBirth')->nullable();
			$table->binary('profilePicture')->nullable();
			$table->timestamp('register_TS')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->string('email')->unique();
			$table->timestamp('lastLogin_TS')->nullable();
            $table->rememberToken();
            $table->timestamps();
            $table->primary('uid');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('users');
	}

}
