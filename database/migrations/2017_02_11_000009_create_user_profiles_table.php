<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('userId', 46)->index();
            $table->string('profileImagePath', 256)->nullable();
            $table->integer('weight')->nullable();
            $table->string('height', 11)->nullable();
            $table->text('about', 65535)->nullable();
            $table->string('favWorkout')->nullable();
            $table->string('favQuote')->nullable();
            $table->string('location')->nullable();
            $table->timestamps();

            $table->foreign('userId')->references('uid')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_profiles');
    }
}
