<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExerciseStatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exercise_stats', function (Blueprint $table) {
            $table->increments('id');
            $table->string('uid',46)->index();
            $table->integer('exerciseId')->unsigned()->index();
            $table->integer('workoutStatId')->unsigned()->index();
            $table->float('distance', 6, 3)->nullable();
            $table->timestamp('start_TS')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('end_TS')->nullable();
            $table->timestamps();

            $table->foreign('exerciseId')->references('id')->on('exercises');
            $table->foreign('workoutStatId')->references('id')->on('workout_stats')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exercise_stats');
    }
}
