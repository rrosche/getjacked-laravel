<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProgramStatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('program_stats', function (Blueprint $table) {
            $table->increments('id');
            $table->string('uid',46)->index();
            $table->integer('programId')->unsigned()->index();
            $table->integer('completionPercent');
            $table->dateTime('Start_TS')->nullable();
            $table->dateTime('End_TS')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('program_stats');
    }
}
