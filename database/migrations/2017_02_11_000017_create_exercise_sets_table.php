<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExerciseSetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exercise_sets', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('exerciseStatId')->unsigned()->index();
            $table->boolean('order');
            $table->boolean('repetitions');
            $table->smallInteger('weight');
            $table->timestamps();

            $table->foreign('exerciseStatId')->references('id')->on('exercise_stats')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exercise_sets');
    }
}
