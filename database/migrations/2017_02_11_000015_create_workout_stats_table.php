<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkoutStatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('workout_stats', function (Blueprint $table) {
            $table->increments('id');
            $table->string('uid',46)->index();
            $table->integer('workoutId')->unsigned()->index();
            $table->integer('completionPercent');
            $table->dateTime('start_TS')->nullable();
            $table->dateTime('end_TS')->nullable();
            $table->timestamps();

            $table->foreign('uid')->references('uid')->on('users')->onDelete('cascade');
            $table->foreign('workoutId')->references('id')->on('user_workouts')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('workout_stats');
    }
}
