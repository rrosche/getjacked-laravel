<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Entities\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'uid' =>  uniqid('',true),
        'username' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Entities\Exercise::class, function(Faker\Generator $faker){
    return [
        'name' => $faker->name,
        'description'=>$faker->sentences
    ];
});

$factory->define(App\Entities\ExerciseType::class, function(Faker\Generator $faker){
    return [
        'name' => $faker->name
    ];
});

$factory->define(App\Entities\WorkoutType::class, function(Faker\Generator $faker){
    return [
        'name' => $faker->name
    ];
});

$factory->define(App\Entities\ProgramType::class, function(Faker\Generator $faker){
    return [
        'name' => $faker->name
    ];
});

$factory->define(App\Entities\MuscleGroup::class, function(Faker\Generator $faker){
   return [
     'name' => $faker->name
   ];
});

$factory->define(App\Entities\Exercise::class, function(Faker\Generator $faker){
   return [
       'name' => $faker->name,
       'type' => function(){
            return factory(App\Entities\ExerciseType::class)->create()->id;
       },
       'muscleGroup' => function(){
           return factory(App\Entities\MuscleGroup::class)->create()->id;
       },
       'description' => $faker->sentences(1, true),
       'creatorId' => function(){
           return factory(App\Entities\User::class)->create()->uid;
       }

   ];
});

$factory->define(App\Entities\Workout::class, function(Faker\Generator $faker){
    return [
        'name' => $faker->name,
        'type' => function(){
            return factory(App\Entities\WorkoutType::class)->create()->id;
        },
        'creatorId' => function(){
            return factory(App\Entities\User::class)->create()->uid;
        },
        'description' => $faker->sentences(3,true),
        'length' => $faker->numberBetween(0,120),
        'schedule' => $faker->dayOfWeek,
    ];
});

$factory->define(App\Entities\Program::class, function(Faker\Generator $faker){
    return [
        'name' => $faker->name,
        'type' => function(){
            return factory(App\Entities\ProgramType::class)->create()->id;
        },
        'creatorId' => function(){
            return factory(App\Entities\User::class)->create()->uid;
        },
        'description' => $faker->sentences(3,true),
        'length' => $faker->numberBetween(0,120),
        'schedule' => $faker->dayOfWeek,
    ];
});
